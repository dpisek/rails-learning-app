class CreateProducts < ActiveRecord::Migration[7.0]
  def change
    create_table :products do |t|
      t.string :title
      t.text :description
      t.string :image_url
      # 8 digits of significance and 2 after the decimal
      t.decimal :price, precision: 8, scale: 2
      t.timestamps
    end
  end
end
