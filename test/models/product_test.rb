require "test_helper"

class ProductTest < ActiveSupport::TestCase

  test 'product is not valid without a unique title' do
    # title from fixtures, already in DB, so not unique
    existing_title = products(:ruby).title
    product = Product.new(title: existing_title,
                          description: 'yyy',
                          price: 1,
                          image_url: 'fred.gif')
    assert product.invalid?
    assert_equal [I18n.translate('errors.messages.taken')], product.errors[:title]
  end

  test 'product attribute must not be empty' do
    product = Product.new
    assert product.invalid?
    assert product.errors[:title].any?
    assert product.errors[:description].any?
    assert product.errors[:price].any?
    assert product.errors[:image_url].any?
  end

  test 'product price must be positive' do
    product = Product.new(title: 'My Book Title',
                          description: 'yyy',
                          image_url: 'zzz.jpg')

    ok = [1, 10000.01]
    bad = [-1, 0]

    ok.each do |price|
      product.price = price
      assert product.valid?
    end

    bad.each do |price|
      product.price = price
      assert product.invalid?
      assert_equal ['must be greater than or equal to 0.01'],
                   product.errors[:price]
    end
  end

  def new_product(image_url)
    Product.new(title: 'My Book Title',
                description: 'yyy',
                price: 1,
                image_url: image_url)
  end

  test 'image_url' do
    ok = %w( fred.gif fred.jpg fred.png FRED.JPG Fred.Jpg http://a.b.c/7/x/fred.gif )
    bad = %w( fred.doc fred.gif/more fred.gif.more )

    ok.each do |image_url|
      assert new_product(image_url).valid?,
             "#{image_url} must be valid"
    end

    bad.each do |image_url|
      assert new_product(image_url).invalid?,
             "#{image_url} must be invalid"
    end
  end
end
