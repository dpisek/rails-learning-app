class Product < ApplicationRecord
  validates :title, uniqueness: true
  validates :title, :description, :image_url, presence: true
  # using cent values, because if we used 0, then something like 0.0001 would be accepted but stored as 0 in the DB
  validates :price, numericality: { greater_than_or_equal_to: 0.01 }
  validates :image_url, allow_blank: true, format: {
    # the \z ensures it is at the end of the URL
    with: /\.(gif|jpg|png)\z/i,
    message: 'must be a URL for GIF, JPG or PNG image.'
  }
end
